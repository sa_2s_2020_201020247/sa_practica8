# PRACTICA 8

_En esta practica se basa en la creación de un sitio web y su base de datos con docker-compose._

_Como se puede ver en el repositorio, se tiene la siguiente estructura_

```bash
.
├── apache
│   ├── demo.apache.conf
│   └── Dockerfile
├── database
│   ├── Dockerfile
│   └── init.sql
├── docker-compose.yml
├── pagina
│   └── index.php
├── php
│   └── Dockerfile
```

## Información General
- Practica 8 Laboratorio Software Avanzado
- Creado por Haroldo Arias
- Carnet 201020247
- Septiembre 2020
- Ver el video para ver la demostración para mayor explicación

## Video de Demostración
_Video de la demostración de la funcionalidad de estos servicios, como una rápida explicacion de su estructura_
* [Video](https://drive.google.com/file/d/1rzwV5ZHKKkZuMv9lmpYavDe6GYPxlbj9/view?usp=sharing) - Acá puedes verlo

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Pre-requisitos 📋
_Debes de tener instalado Docker y Docker Compose_

## Ejecución del DockerCompose
_Únicamente basta con descargar los archivos, ingresar a una consola y situarnos en la ruta donde descargamos los archivos._
Manualmente hay que correr el siguiente comando

```
docker-compose up -d --build
```

## Autor ✒️

* **Haroldo Pablo Arias Molina** - *Trabajo Inicial* - [harias25](https://github.com/harias25)
 
## Licencia 📄

Este proyecto está bajo la Licencia Libre - mira el archivo [LICENSE.md](LICENSE.md) para detalles